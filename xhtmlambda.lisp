;;;; -*- Mode: Lisp -*-

;;;; XHTMLambda.lisp --
;;;; Yet another^n (X)HTML producing library.
;;;;
;;;; See file COPYING for copyright and license information.


(in-package "XHTMLambda")


;;; Parameters.
;;; ===========

(defvar *xhtml-indent* 0)


(defvar *xhtml-pd* (copy-pprint-dispatch))


(defparameter *downcase-names* t)


;;; http://stackoverflow.com/questions/3558119/are-non-void-self-closing-tags-valid-in-html5
;;; For backwards compatibility we set it to T, but we can change it
;;; if we need HTML5 style closing tags


(defparameter *html5-no-self-closing-tag* 'nil)


;;; https://www.w3.org/TR/html5/syntax.html#void-elements
;;; Void elements only have a start tag; end tags must not be
;;; specified for void elements.

(defvar *html5-void-elements*
  '(area
    base
    br
    col
    embed
    hr
    img
    input
    keygen
    link
    meta
    param
    source
    track
    wbr))


;;;; Main definitions.
;;;; =================
;;;;
;;;; The realization is by SEXPs translated into the ELEMENT structures below.

;;;; element --

(defstruct (element
            (:constructor %element)
            )
  "The ELEMENT Structure.

This structure represents a minimal 'XML' element with a TAG, a (LIST)
of ATTRIBUTES and relative CONTENT."
  (tag nil :type symbol :read-only t)
  (attributes () :type list :read-only t)
  (content () :read-only t)
  )


;;;; element --

(defgeneric element (tag attributes &rest content)
  (:documentation "The ELEMENT constructor.

Serves as a 'factory' for ELEMENT objects and as a hook for peri-call
methods.")

  (:method ((tag symbol) (attributes list) &rest content)
   (%element :tag tag
             :attributes attributes
             :content (splice-element-sublists content)))
  )


;;;; is-html-content --

(defun is-html-content (x)
  (or (element-p x) (stringp x)))


;;;; splice-element-sublist --

(defun splice-element-sublists (le)
  (loop for e in le
        if (and (listp e)
                (every 'is-html-content e))
          nconc e
        else
          collect e))


;;; tag --
;;; The tag structure contains (meta) information about a specific tag.
;;; Cfr. the HTML5 spec for the details on each field.  Most of them
;;; are kept empty and for future use.

(defstruct (tag (:conc-name t_))
  "The Tag Structure.

The tag structure contains (meta) information about a specific tag.
Cfr. the HTML5 spec for the details on each field.  Most of them
are kept empty and for future use."
  (name nil :type (or symbol string) :read-only t)
  (categories () :read-only t)
  (context () :read-only t)
  (content () :read-only t)
  (specific-attributes () :read-only t)
  )


;;; xhtml-parse-error --

(define-condition xhtml-parse-error (parse-error)
  ((tag :reader xhtml-parse-error-tag
        :initarg :tag)
   (msg :reader xhtml-parse-error-msg
        :initarg :message
        :type string
        :initform "")
   )
  (:report (lambda (c s)
             (format s "(X)HTML parse error for tag ~A~%~A"
                     (xhtml-parse-error-tag c)
                     (xhtml-parse-error-msg c))))
  (:documentation
   "The XHTML Parse Error Condition.

This is the (sub)root of the parse errors related to (X)HTML, HTML5
and XML parsing."
   )
  )


;;;; Tag handling.
;;;; -------------

;;;; *html-tags* --

(defparameter *html-tags* ())


;;;; *html-tags-db* --

(defparameter *html-tags-db* (make-hash-table :test #'equalp))


;;;; Tag accessors
;;;; The information about the "structure" of a tag is indexed on the
;;;; tag name (and the index is the *HTML-TAGS-DB* hash-table.

(declaim (inline tag-info)
         (ftype (function ((or string symbol))
                          (values (or null tag)
                                  boolean))
                tag-info))

(defun tag-info (name)
  (declare (type (or string symbol) name))
  (gethash (string name) *html-tags-db*))


(defun is-html-tag (x)
  (declare (type (or string symbol) x))
  (not (null (tag-info x))))


(defun tag-name (name)
  (declare (type (or string symbol) name))
  (let ((tag (tag-info name)))
    (if tag
        (values (t_name tag) t)
        (values nil nil))))


(defun tag-categories (name)
  (declare (type (or string symbol) name))
  (let ((tag (tag-info name)))
    (if tag
        (values (t_categories tag) t)
        (values nil nil))))


(defun tag-context (name)
  (declare (type (or string symbol) name))
  (let ((tag (tag-info name)))
    (if tag
        (values (t_context tag) t)
        (values nil nil))))


(defun tag-content (name)
  (declare (type (or string symbol) name))
  (let ((tag (tag-info name)))
    (if tag
        (values (t_content tag) t)
        (values nil nil))))


(defun tag-specific-attributes (name)
  (declare (type (or string symbol) name))
  (let ((tag (tag-info name)))
    (if tag
        (values (t_specific-attributes tag) t)
        (values nil nil))))


;;;; is-html-sexp --

(defgeneric is-html-sexp (sexp)

  (:method ((sexp symbol))
   (is-html-tag sexp))

  (:method ((sexp cons))
   (typecase (first sexp)
     (symbol (is-html-sexp (first sexp)))
     (cons (let ((tag (caar sexp)))
             (and (symbolp tag)
                  (is-html-sexp tag))))
     ))
  )


;;;; Translation.
;;;; ------------
;;;;
;;;; Translating a XHTML-SEXP into a hierarchical set of ELEMENT

;;; translate-xhtml-sexp --
;;; returns a ELEMENT or a STRING.

(defgeneric translate-xhtml-sexp (xhtml-sexp))

(defmethod translate-xhtml-sexp ((xhtml-sexp symbol))
  (if (is-html-tag xhtml-sexp)
      `(element ',xhtml-sexp nil)
      xhtml-sexp
      ))


(defmethod translate-xhtml-sexp ((xhtml-sexp character))
  `(string ',xhtml-sexp)
  )


(defmethod translate-xhtml-sexp ((xhtml-sexp string))
  xhtml-sexp
  )

(defmethod translate-xhtml-sexp ((xhtml-sexp number))
  `(princ-to-string ',xhtml-sexp) ; This should be more
                                  ; discriminating, in order to print
                                  ; numbers better suitable for XHTML.
  )


;;; In this method we need the usual munging to accommodate different
;;; syntaxes for attributes. For the time being the only syntaxes are
;;;
;;;    (tag . content)
;;;
;;; and
;;;
;;;    ((tag . attributes) . content)
;;;
;;; TAG is a symbol, yadda, yadda, yadda... This is pretty "conventional" at
;;; this point.
;;;
;;; However, the (CONS SYMBOL) branch should not be called at all if
;;; we use HTMLIZE.

(defmethod translate-xhtml-sexp ((xhtml-sexp cons))
  (destructuring-bind (tag &rest spec)
      xhtml-sexp
    (etypecase tag
      (symbol `(apply #'element ',tag
                      (translate-xhtml-attributes ',tag
                                                  ,(first spec))
                      (list ,@(rest spec))))
      ((cons symbol)
       `(apply #'element
               ',(first tag)
               (translate-xhtml-attributes ',(first tag)
                                           (list ,@(rest tag)))
               (list ,@spec)))
      )))


;;; translate-xhtml-attributes --
;;; The result is an alist of string keys and values.

(defun translate-xhtml-attributes (tag attributes)
  (declare (ignore tag))

  (assert (evenp (list-length attributes)))
  (assert (loop for x in attributes by #'cddr
                always (typep x '(or string symbol))))

  (loop for (attr val) on attributes by #'cddr
        for attr-name = (if *downcase-names*
                            (string-downcase (string attr))
                            (string attr))
        nconc (list attr-name val)))


;;;; ensure-evaluation --
;;;; Ensures that TRANSLATE-XHTML-SEXP is called on symbols that are
;;;; html tags.  In other words this is where the "partial evaluation"
;;;; of html forms comes in.

(defun ensure-evaluation (sexp)
  (typecase sexp
    (symbol
     (if (is-html-sexp sexp)
         (translate-xhtml-sexp sexp)
         sexp))
    ((cons cons)
     (translate-xhtml-sexp (cons (first sexp)
                                 (mapcar #'ensure-evaluation
                                         (rest sexp)))))

    #|((cons symbol)
     (if (is-html-tag (first sexp))
         `(translate-xhtml-sexp (cons ,(first sexp)
                                      (mapcar #'ensure-evaluation
                                              ,(rest sexp))))
         sexp))|#

    (t sexp))
  )


;;;; Syntax.
;;;; =======

;;; html-sexp-syntax --

(deftype html-sexp-syntax ()
  "The (enumerated) type of 'syntaxes' for the HTML Sexp-based representation.

The actual type is defined to be (MEMBER :COMPACT :STANDARD).
Each syntax style allows for different ways to write (X)HTML in Common Lisp code.

Examples:

:COMPACT
    tag
    (tag . body)
    ((tag . attribute) . body)

:STANDARD
    tag
    (tag attributes . body)
"
  '(member :compact :standard))


;;;; *default-html-sexp-syntax* --

(defparameter *default-html-sexp-syntax* :compact
  "The default HTML syntax used in the SEXP-based coding of HTML.

The variable can take on the values of the type HTML-SEXP-SYNTAX,
i.e., :COMPACT or :STANDARD.  The default is :COMPACT.

See Also:

Type HTML-SEXP-SYNTAX")


;;;; rewrite --

(defgeneric rewrite (op spec syntax)
  (:documentation "Rewrites a Sexp representing a (X)HTML element.")
  )


;;;; rewrite-form --

(defun rewrite-form (form &optional (syntax :compact))
  (if (consp form)
      (rewrite (first form) (rest form) syntax)
      form))


;;;; rewrite methods --

(defmethod rewrite ((op cons) (spec list) (syntax (eql :compact)))
  (let ((tag-name (first op))) ; This is surely a (X)HTML form.
    (cond ((tag-content tag-name)
           (list* tag-name
                  (rest op)
                  (mapcar (lambda (form)
                            (rewrite-form form syntax))
                          spec)))
          (spec
           (error 'xhtml-parse-error
                  :tag tag-name
                  :message "The tag cannot have content; cannot rewrite.")) ; No content!!!!!

          (t
           (cons tag-name (rest op)))
          )))


(defmethod rewrite ((op symbol) (spec list) (syntax (eql :compact)))
  (if (is-html-tag op)
      (let ((rewritten-spec
             (mapcar (lambda (form)
                       (rewrite-form form :compact))
                     spec))
            )
        (if (tag-content op)
            (list* op () rewritten-spec)
            (cons op rewritten-spec)))
      (cons op spec)))


#+xhtml-strict-rewrite
(defmethod rewrite ((op cons) (spec list) (syntax (eql :standard)))
  (let ((op-op (first op)))
    (if (eq op-op 'lambda) ; Special case...  Could be made less stringent.
        (cons op spec)
        (error 'parse-error))))


#-xhtml-strict-rewrite
(defmethod rewrite ((op cons) (spec list) (syntax (eql :standard)))
  (let ((op-op (first op)))
    (if (is-html-tag op-op)
        (error 'xhtml-parse-error
               :tag (first op)
               :message (format nil
                                "Syntax ((<tag> . <attributes>) . <content>) is not allowed with :STANDARD syntax."))
        (cons op spec))))


(defmethod rewrite ((op symbol) (spec list) (syntax (eql :standard)))
  (if (is-html-tag op)
      (if (tag-content op)
          (let ((rewritten-spec
                 (mapcar (lambda (form)
                           (rewrite-form form :standard))
                         spec))
                )
            (list* op
                   (first rewritten-spec)
                   (rest rewritten-spec))
            (cons op rewritten-spec))
          (cons op spec))
      (cons op spec)))


(defun rewrite-xhtml-form (xhtml-form &optional (syntax *default-html-sexp-syntax*))
  (declare (type html-sexp-syntax syntax))
  (flet ((rxf (form) (rewrite-xhtml-form form syntax)))
    (ecase syntax
      (:compact
       (if (consp xhtml-form)
           (let ((op (first xhtml-form)))
             (etypecase op
               (symbol
                (if (is-html-tag op)
                    (list* op
                           '()
                           (mapcar #'rxf (rest xhtml-form)))
                    xhtml-form))
               ((cons symbol)
                (list* (first op)
                       (rest op)
                       (mapcar #'rxf (rest xhtml-form))))))
           xhtml-form))

      (:standard
       (if (consp xhtml-form)
           (let ((op (first xhtml-form)))
             (etypecase op
               (symbol xhtml-form)
               ((cons symbol)
                (if (eq (first op) 'lambda)
                    xhtml-form
                    (error 'xhtml-parse-error :tag (first op))))
               ))
           xhtml-form))
      )))


(defmacro htmlize (xhtml-form &key (syntax *default-html-sexp-syntax*))
  "The HTMLIZE macro.

Just a synonim for HTMLISE.

Notes:

The Bard is having precedence.

See Also:

HTMLISE, HTMLIZE*
"
  (declare (type html-sexp-syntax syntax))
  (rewrite-form xhtml-form syntax))


(defmacro htmlise ((&key (syntax *default-html-sexp-syntax*)) xhtml-form)
  "The HTMLISE macro.

The macro 'rewrites' a form according to the SYNTAX switch.  If it is
:COMPACT then all the forms are read as if they where written in the
abbreviated syntax, which is more useful for many forms without too
many attributes.  Otherwise, the 'standard' syntax is used, where the
attribute list must always be specified, even if ().  The default for
the SYNTAX switch, which is held in the variable
*DEFAULT-HTML-SEXP-SYNTAX* is :COMPACT.

Arguments and Values:

XHTML-FORM : a Sexp representing some (X)HTML code.
SYNTAX : a HTML-SEXP-SYNTAX designator; defaults to *DEFAULT-HTML-SEXP-SYNTAX*
FORM : a FORM

Examples:

cl-prompt> (htmlise (:syntax :compact) (<:p (<:b \"Compact syntax.\")))
<p><b>Compact Syntax</b></p>

cl-prompt> (htmlise (:syntax :standard) (<:p () (<:b () \"Standard syntax.\")))
<p><b>Standard syntax</b></p>

cl-prompt> (htmlise () ((<:p :style \"color: red\") (<:b \"Compact syntax.\")))
<p style=\"color: red\"><b>Compact Syntax</b></p>

See Also:

*default-html-sexp-syntax*, HTMLIZE, HTMLIZE*
"
  (declare (type html-sexp-syntax syntax))
  (rewrite-form xhtml-form syntax))


(defun htmlize* (xhtml-form &key (syntax *default-html-sexp-syntax*))
  "This is the functional equivalent of the HTMLISE macro.

See Also:

HTMLISE, HTMLIZE"
  (declare (type html-sexp-syntax syntax))
  (rewrite-form xhtml-form syntax))


;;;; Now we do TRT.  We define a bunch of macros that represent the
;;;; XHTML tags.  Each "expands" in a call to the appropriate
;;;; "element"

;;;; Each macro respects the dictates of
;;;; http://www.w3.org/TR/2008/WD-html5-20080122/

;;;; Writing the document will respect the same document.
;;;; Especially w.r.t. Section 8 "The HTML syntax"

;;;; Note:  Here is a "definer".

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defvar *def-el-v* (gensym "DEF-EL-V-")))


(defmacro def-element (name &key
                            (categories ())
                            (context ())
                            (content t)
                            (specific-attributes ())
                            (documentation)
                            )
  (let ((v *def-el-v*)
        (is-name (intern (format nil "IS-~A" name))) ; Yeah, yeah: a non-gensymmed var.
        )
    `(eval-when (:load-toplevel :execute)
       ;; Some housekeeping...
       (pushnew ',name *html-tags*)
       (setf (gethash (string ',name) *html-tags-db*)
             (make-tag :name ',name
                       :categories ',categories
                       :content ',content
                       :context ',context
                       :specific-attributes ',specific-attributes))
       (export (list ',name ',is-name) "XHTMLAMBDA")


       ,(if content
            `(defmacro ,name (attributes &rest content)
               ,documentation
               (let ((,v ',name))
                 (translate-xhtml-sexp
                  `(,,v
                    ,(when attributes `(list ,@(ensure-evaluation attributes)))
                    ,.(mapcar #'ensure-evaluation content)))))

            `(defmacro ,name (&rest content)
               ,documentation
               (let ((,v ',name))
                 (translate-xhtml-sexp
                  `(,,v
                    (list ,@(mapcar #'ensure-evaluation content))))))
            )
       (defun ,is-name (x)
         (and (element-p x)
              (eq (element-tag x) ',name)))
       )))


;;; HTML5 elements.
;;; ===============
;;;
;;; Some of the elements are left "as they are" without using the macro
;;; DEF-ELEMENT.  They were built befere the macro was written and
;;; they served as its basis.  Their definition is likely to disappear in
;;; the future.


#|
;;; The following, commented, code is the template used for DEF-ELEMENT.
;;; It is left here for future memory....

;;; root element

(defmacro html (attributes head body)
  (translate-xhtml-sexp `(html
                          ,(when attributes `(list ,@(ensure-evaluation attributes)))
                          ,(ensure-evaluation head)
                          ,(ensure-evaluation body))))

(export '(html))


;;; metadata

(defmacro head (attributes &rest content)
  (translate-xhtml-sexp `(head
                          ,(when attributes `(list ,@(ensure-evaluation attributes)))
                          ,.(mapcar #'ensure-evaluation content))))
(export '(head))


;;; base

(defmacro base (&rest content) ; 'base' has no contents
  (translate-xhtml-sexp `(base
                          ,(mapcar #'ensure-evaluation content))))
(export '(base))
|#

(def-element html
             :documentation "The top level HTML element.")
(def-element head)
(def-element title)
(def-element base :content nil)
(def-element link :content nil)
(def-element meta :content nil)
(def-element style)


;;; sections

(def-element body)
(def-element section)
(def-element nav)
(def-element article)
(def-element aside)
(def-element h1)
(def-element h2)
(def-element h3)
(def-element h4)
(def-element h5)
(def-element h6)
(def-element header)
(def-element main)
(def-element footer)
(def-element address)


;;; prose

(def-element p)
(def-element hr :content nil)
(def-element br :content nil)
(def-element dialog)


;;; preformatted text

(def-element pre)


;;; lists

(def-element ol)
(def-element ul)
(def-element li)
(def-element dl)
(def-element dt)
(def-element dd)


;;; phrase elements

(def-element a)
(def-element q)
(def-element cite)
(def-element em)
(def-element strong)
(def-element small)
(def-element m)
(def-element dfn)
(def-element abbr)

(progn  ; !!!!! Needs shadowed, special treatment in HTMLIZE.
  (def-element time)
  (unexport (list 'time) "XHTMLAMBDA"))

(def-element progress)
(def-element meter)
(def-element code)
(def-element var)
(def-element samp)
(def-element kbd)
(def-element sub)
(def-element sup)
(def-element span)
(def-element b)
(def-element bdo)


;;; Other elements...

(def-element i)
(def-element tt)


;;; edits.

(def-element ins)
(def-element del)


;;; embedded

(def-element figure)
(def-element img :content nil)
(def-element iframe)
(def-element embed)
(def-element object)
(def-element param)
(def-element video)
(def-element audio)
(def-element source :content nil)
(def-element canvas)

(progn ; !!!!!!!! Need special treatment in HTMLIZE.
  (def-element map)
  (unexport (list 'map) "XHTMLAMBDA"))

(def-element area :content nil)


;;; tabular data

(def-element table)
(def-element caption)
(def-element colgroup)
(def-element col :content nil)
(def-element tbody)
(def-element thead)
(def-element tfoot)
(def-element tr)
(def-element td)
(def-element th)


;;; forms

(def-element form)
(def-element fieldset)
(def-element input)
(def-element button)
(def-element label)
(def-element select)
(def-element datalist)
(def-element optgroup)
(def-element option)
(def-element textarea)
(def-element output)


;;; scripting

(def-element script)
(def-element noscript)
(def-element event-source :content nil)
(def-element details)
(def-element datagrid)
(def-element command :content nil)
(def-element menu)


;;; data templates

(def-element datatemplate)
(def-element rule)
(def-element nest)


;;; miscellaneous

(def-element legend)
(def-element div)


;;;; Framesets and frames; not in HTML5.
;;;; -----------------------------------

(def-element frameset)
(def-element frame)
(def-element noframes)


;;;; Non standard.
;;;; -------------

(def-element comment)
(def-element document) ; A (X)HTML "full" document with all the
                       ; preliminary comments and DOCTYPE declarations.


;;;; Output.
;;;; =======
;;;;
;;;; The implementation uses the pretty printer.  Cfr. Water's work.

(defparameter *quote-character* #\"
  "The 'quote' character used in (X)HTML(5) string quoting.

The default is #\\\".
")


(defun set-quote-character (c)
  "Setf the current 'quote' character used in (X)HTML(5) string quoting.

Arguments and Values:

C : a character, either #\' or #\\\".
"
  (declare (type character c))
  (assert (or (char= c #\") (char= c #\')))
  (setq *quote-character* c))


(defmethod print-object ((e element) s)
  (declare (type stream s))
  (let ((tag (element-tag e))
        (attributes (element-attributes e))
        (content (element-content e))
        )
    (cond (*print-pretty*
           (pprint-xhtml s e))

          (*print-readably*
           (format s "#S(~S :TAG ~S :ATTRIBUTES ~:S :CONTENT ~:S)"
                   (type-of e)
                   tag
                   attributes
                   content))

          (t
           ;; Format string showing-off!!!!
           (format s "<~A~{ ~A=\"~A\"~}~:[ />~;>~:*~{~A~^ ~}</~3:*~A>~]"
                   (string-downcase tag)
                   attributes
                   content
                   )
           ))
    ))


(defmacro with-html-syntax ((stream &key
                                    (print-pretty *print-pretty*))
                            html)
  `(write ,html
          :stream ,stream
          :pprint-dispatch *xhtml-pd*
          :pretty ,print-pretty
          ))


(defmacro with-html-syntax-output ((stream
                                    &key
                                    (print-pretty *print-pretty*)
                                    (syntax *default-html-sexp-syntax*)
                                    )
                                   html)
  "Writes out a (X)HTML form to a stream.

The writing is done using the XHTMLambda pretty-print dispatch table,
which is used if PRINT-PRETTY is non-NIL.  The HTML form is
represented using the appropriate SYNTAX.

Arguments and Values:

STREAM : a STREAM
PRINT-PRETTY : a generalized boolean; defaults to *print-pretty*
SYNTAX : a HTML-SEXP-SYNTAX value; defaults to *default-html-sexp-syntax*
FORM : a FORM

Examples:

cl-prompt> (<:with-html-syntax-output (*standard-output* :syntax :standard
                                                         :print-pretty t)
               (<:p () (<:code () \"for i in $ZZ do ...\")))
<p>
  <code>
      for i in $ZZ do ...
  </code>
</p>
<p><code>for i in $ZZ do ...</code></p>

See Also:

*default-html-sexp-syntax* and html-sexp-syntax
"
  `(write (htmlize ,html :syntax ,syntax)
          :stream ,stream
          :pprint-dispatch *xhtml-pd*
          :pretty ,print-pretty
          ))


(defun pprint-xhtml (s xhtml-element)
  (declare (type stream s)
           (type element xhtml-element))
  (let ((tag (string-downcase (element-tag xhtml-element)))
        (attrs (element-attributes xhtml-element))
        (content (element-content xhtml-element))
        (html5-void-tag  (member (element-tag xhtml-element) *html5-void-elements*)))
    ;; (cerror "examining tag closing" "data ~a ~a" s xhtml-element)
    (pprint-logical-block (s content)
      (pprint-logical-block (s content)
        ;; (write-string "<" s)
        ;; (write-string tag s)
        ;; (format s "<~A" tag)
        ;; (format s "<~A~@<~{~^ ~A=\"~S\"~^~_~}~:>" tag attrs)
        (format s "<~A~@<~{~^ ~A=\"~A\"~^~_~}~:>" tag attrs)

        (when (or content *html5-no-self-closing-tag*)
          (write-char #\> s)
          ;; (pprint-indent :block 4 s)
          (when content (pprint-newline :mandatory s))
          (format s "~{~4,0:T~:W~_~}" content)
          ;; (format s "~{~:W~_~}" content)
          ;; (pprint-indent :block -4 s)
          ))
      (cond (content
             (format s "~0I</~A>" tag))
            ((and (null content) *html5-no-self-closing-tag*)
             (unless html5-void-tag ; We don't close html5 void elements
               (format s "</~A>" tag)))
            (T
             (write-string " />" s)))
      )))


(defun pprint-xhtml-string (s xhtml-string)
  (declare (type string xhtml-string))
  (write-string xhtml-string s))


(defun pprint-xhtml-symbol (s xhtml-symbol)
  (declare (type symbol xhtml-symbol))
  (write-string (string xhtml-symbol) s))


(defun pprint-xhtml-comment (s xhtml-comment)
  (format s "<!-- ~{~A ~}-->"
          (element-content xhtml-comment)))


(defun pprint-xhtml-document (s xhtml-document)
  (format s "~{~&~:W~}"
          (element-content xhtml-document)))


(defun pprint-xhtml-html (s xhtml-document) ; Just to get rid of some indentation.
  (format s "~&<html>~{~&~:W~}~&</html>~%"
          (element-content xhtml-document)))


(defun pprint-xhtml-pre (s xhtml-pre)
  (let ((*print-pretty* nil))
    (format s "<pre>~{~&~:W~}</pre>" (element-content xhtml-pre))))


(eval-when (:load-toplevel :execute)
  (set-pprint-dispatch 'element
                       'pprint-xhtml
                       0
                       *xhtml-pd*)

  (set-pprint-dispatch 'string
                       'pprint-xhtml-string
                       0
                       *xhtml-pd*)

  (set-pprint-dispatch '(satisfies is-comment)
                       'pprint-xhtml-comment
                       1
                       *xhtml-pd*)

  (set-pprint-dispatch '(satisfies is-document)
                       'pprint-xhtml-document
                       1
                       *xhtml-pd*)

  (set-pprint-dispatch '(satisfies is-html)
                       'pprint-xhtml-html
                       1
                       *xhtml-pd*)

  (set-pprint-dispatch '(satisfies is-pre)
                       'pprint-xhtml-pre
                       1
                       *xhtml-pd*)

  (set-pprint-dispatch 'symbol
                       'pprint-xhtml-symbol
                       0
                       *xhtml-pd*)
  )


;;;; end of file -- xhtmlambda.lisp
