;;;; -*- Mode: Lisp -*-

;;;; lispworks.lisp --

;;;; XHTMLambdag Lispworks dependent code.

(editor:setup-indent "with-html-syntax" 1 4 6)
(editor:setup-indent "with-html-syntax-output" 1 4 6)

(editor:setup-indent "htmlise" 1 4 6)

;;;; lispworks.lisp --
