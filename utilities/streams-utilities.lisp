;;; -*- Mode: Lisp -*-

;;;; streams-utilities.lisp --
;;;; get-unget-stream.lisp -- Original file name...

#| (in-package "CL.EXTENSIONS.STREAMS") |# ; Copied from there!

(in-package "XHTMLambda")


(defparameter *backtraceable-streams*
  (make-hash-table :test #'eq))


(deftype backlog-length ()
  (list 'integer 0 most-positive-fixnum))

(defparameter *backlog-length* 1024)


(declaim (type backlog-length *backlog-length*)
         (type hash-table *backtraceable-streams*))


(declaim (inline stream-backlog
                 (setf stream-backlog)
                 get-char
                 unget-char))


(declaim (ftype (function (stream) list) stream-backlog))


(defun stream-backlog (stream)
  (gethash stream *backtraceable-streams* ()))


(defun (setf stream-backlog) (v stream)
  (declare (type list v))
  (setf (gethash stream *backtraceable-streams*) v))


(defun clean-stream-backlog (stream)
  (remhash stream *backtraceable-streams*))


(defun clean-streams-backlogs ()
  (clrhash *backtraceable-streams*))


(defmacro with-backlogged-stream ((s) &body forms)
  `(unwind-protect
       ,@forms
     (remhash ,s *backtraceable-streams*)))
  

(defvar *whitespace-characters* ; XML characters.
  (list (code-char #x20) ; #\Space
        (code-char #x9)  ; #\Tab
        (code-char #xD)  ; #\Return
        (code-char #xA)  ; #\Newline
        ))


(declaim (type list *whitespace-characters*))


(defun look-at-char (&optional peek-type ; Roll it back in the original file!!!!
                               (stream *standard-input*)
                               (eof-error-p t)
                               eof-value
                               recursive-p)
  (declare (type (or boolean character) peek-type)
           (type stream stream)
           (type boolean eof-error-p recursive-p))
  (let ((stream-backlog (stream-backlog stream)))
    (declare (type list stream-backlog))
    (when stream-backlog
      (cond ((null peek-type)
             (return-from look-at-char (first stream-backlog)))

            ((eq peek-type t)
             (loop for c of-type (or null character) = (first stream-backlog)
                   while (and c
                              (member c *whitespace-characters*
                                      :test #'char=))
                   do (pop stream-backlog)
                   finally (when stream-backlog
                             (return-from look-at-char
                               (first stream-backlog))))
             )
            (t ; PEEK-TYPE is a character.
             (loop for c of-type (or null character) = (first stream-backlog)
                   while (and c (char/= c peek-type))
                   do (pop stream-backlog)
                   finally (when stream-backlog
                             (return-from look-at-char
                               (first stream-backlog)))))
            ))

    ;; If we get here, then no RETURN-FROM was reached before, that
    ;; is, either there was no backlog or it was exhausted before
    ;; satisfying the PEEK-TYPE request (cfr., CL:PEEK-CHAR)
    ;; Therefore we resort to CL:PEEK-CHAR.

    (peek-char peek-type stream eof-error-p eof-value recursive-p))
  )


(defun get-char (&optional (stream *standard-input*)
                           (eof-error-p t)
                           eof-value
                           recursive-p)
  (declare (type stream stream)
           (type boolean eof-error-p recursive-p))

  (let ((char-backlog (pop (stream-backlog stream)))) ; Fuck you anaphoric macros! Yeah!
    (declare (type (or null character) char-backlog))
    (if char-backlog
        char-backlog
        (read-char stream eof-error-p eof-value recursive-p))))


(defun unget-char (char &optional
                        (stream *standard-output*)
                        (backlog-length *backlog-length*))
  (declare (type character char)
           (type stream stream))

  (declare (ignore backlog-length)) ; Should check current backlog length.
  (push char (stream-backlog stream))
  nil
  )


(defun unget-chars (chars
                    &optional
                    (stream *standard-output*)
                    (backlog-length *backlog-length*))
  (declare (type (or character list vector) chars)
           (type stream stream)
           (type backlog-length backlog-length))
  (etypecase chars
    (list (dolist (c (reverse chars))
            (declare (type character c))
            (unget-char c stream backlog-length)))
    (vector (loop for c of-type character across (reverse chars)
                  do (unget-char c stream backlog-length)))
    (character (unget-char chars stream backlog-length))
    ))


;;; end of file -- get-unget-stream.lisp --
