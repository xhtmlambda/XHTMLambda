;;;; -*- Mode: Lisp -*-

;;;; xthmlambda-pkg.lisp --
;;;;
;;;; See file COPYING for copyright and license information.

(defpackage "IT.UNIMIB.DISCO.MA.XHTMLAMBDA" (:use "CL")
  (:nicknames "XHTMLAMBDA" "XHTMLambda" "<") ; The last one is a nice trick lifted from YACLML.
  (:documentation "The XHTMLambda Package.

Yet another HTML/XHTML/XML producing (and parsing) library.")
  (:shadow "MAP" "TIME")
  (:export
   "WITH-HTML-SYNTAX"
   "WITH-HTML-SYNTAX-OUTPUT"
   "HTMLIZE"
   "HTMLIZE*"
   "HTMLISE"
   )

  (:export
   "HTML-SEXP-SYNTAX"
   "*DEFAULT-HTML-SEXP-SYNTAX*"
   "*HTML5-NO-SELF-CLOSING-TAG*"
   )

  (:export "PARSE")
  )

;;;; end of file -- xhtmlambda-pkg.lisp --
