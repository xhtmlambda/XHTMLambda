;;;; -*- Mode: Lisp -*-

;;;; xml-parser.lisp --
;;;; Ok.  Enough of other software.  Let's reinvent the wheel.
;;;;
;;;; Start date: 20110917
;;;;
;;;; Reference:
;;;; https://www.w3.org/XML/
;;;; https://www.w3.org/TR/2006/REC-xml11-20060816/
;;;; https://www.w3.org/TR/2008/REC-xml-20081126/
;;;;
;;;; See file COPYING for copyright and licensing information.

(in-package "XHTMLambda")

(eval-when (:load-toplevel :compile-toplevel :execute)
  (shadow '(cl:name-char)))

(deftype character-buffer ()
  `(array character 1))


(declaim (type character-buffer *buffer-collector*))

(defparameter *buffer-collector*
  (make-array 1024
              :fill-pointer 0
              :adjustable t
              :element-type 'character))


(declaim (ftype (function () character-buffer) new-buffer)
         (inline new-buffer))

(defun new-buffer ()
  (make-array 1024
              :fill-pointer 0
              :adjustable t
              :element-type 'character))


(declaim (ftype (function (&optional character-buffer) character-buffer)
                copy-buffer
                reset-buffer)
         (inline copy-buffer
                 reset-buffer))

(defun copy-buffer (&optional (b *buffer-collector*))
  (copy-seq b))


(defun reset-buffer (&optional (b *buffer-collector*))
  (setf (fill-pointer b) 0)
  b)


(defvar *eof* (gensym "EOF"))


(declaim (ftype (function (t) boolean) is-eof)
         (inline is-eof))

(defun is-eof (x)
  (eq x *eof*))


(declaim
 (ftype (function (character
                   &optional
                   stream
                   boolean
                   boolean
                   t
                   boolean)
                  (or character null))
        read-char=)

 (ftype (function (string
                   &optional
                   stream
                   boolean
                   t
                   boolean)
                  (or string null))
        read-chars=))


;;;; 2.1 Well-formed XML Documents

;;; Document

;;; Rule [1]: document

(defun document-rule (xml-stream &optional (b))
  (declare (ignore b))
  (let ((p (prolog xml-stream)))
    (when p
      (let ((e (element-rule xml-stream)))
        (when e
          (let ((m (misc* xml-stream)))
            `(document ,p ,e ,m)))))))


;;;; 2.2 Characters

;;; Characters

;;; Rule [2]: Char
;;; Folded in IS-CHARACTER and COLLECT-CHAR functions.

(declaim (inline is-character
                 is-latin-character
                 is-latin-alphabetic-character
                 collect-char
                 collect-string
                 ))

(defun is-character (x)
  (declare (type character x))
  (let ((cc (char-code x)))
    (or (= cc #x9)
        (= cc #xA)
        (= cc #xD)
        (<= #x20 cc #xD7FF)
        (<= #xE000 cc #xFFFD)
        (<= #x10000 cc #x10FFFF))))


(defun is-latin-character (x)
  (declare (type character x))
  (or (char<= #\a x #\z)
      (char<= #\A x #\Z)
      (char<= #\0 x #\9)))


(defun is-latin-alphabetic-character (x)
  (declare (type character x))
  (or (char<= #\a x #\z)
      (char<= #\A x #\Z)))


(defun collect-char (c buffer)
  (vector-push-extend c buffer)
  c)

(defun collect-string (s buffer)
  (loop for c across s
        do (vector-push-extend c buffer))
  s)


;;;; 2.3 Common Syntactic Constructs.

(declaim (inline is-white-space))

(defun is-white-space (x &aux (cc (char-code x)))
  (declare (type character x))
  (or (= cc #xA)
      (= cc #x20)
      (= cc #x9)
      (= cc #xD)))


;;; White space

;;; Rule [3]: S
;;;
;;; Notes:
;;;
;;; End-of-Line Handling (cfr. 2.11) may be incorrect.

(defun white-space (xml-stream &optional (ws (new-buffer)))
  (let ((nc (get-char xml-stream nil *eof*)))
    (cond ((is-eof nc)
           (copy-buffer ws))

          ((is-white-space nc)
           (collect-char nc ws)
           (white-space xml-stream ws))

          ;; FOLLOW
          ((string= "" ws)
           (unget-char nc xml-stream)
           nil)

          (t
           (unget-char nc xml-stream)
           (copy-buffer ws)))))


(defun s (xml-stream &optional (ws (new-buffer)))
  (white-space xml-stream ws))


;;; Names and Tokens

;;; Rule [4]: NameStartChar

(defun is-name-start-char (c &aux (cc (char-code c)))
  (or (char= c #\:)
      (char<= #\A c #\Z)
      (char= c #\_)
      (char<= #\a c #\z)
      (<= #xC0 cc #xD6)
      (<= #xD8 cc #xF6)
      (<= #xF8 cc #x2FF)
      (<= #x370 cc #x37D)
      (<= #x37F cc #x1FFF)
      (<= #x200C cc #x200D)
      (<= #x2070 cc #x218F)
      (<= #x2C00 cc #x2FEF)
      (<= #x3001 cc #xD7FF)
      (<= #xF900 cc #xFDCF)
      (<= #xFDF0 cc #xFFFD)
      (<= #x10000 cc #xEFFFF)
      ))


(defun name-start-char (xml-stream &optional (b (new-buffer)))
  (let ((nc (get-char xml-stream nil nil)))
    (when nc
      (if (is-name-start-char nc)
          (collect-char nc b)
          (unget-char nc xml-stream)))))


;;; Rule [4a]: NameChar
;;;
;;; Notes:
;;;
;;; Order different just for "microefficiency".

(defun is-name-char (c &aux (cc (char-code c)))
  (or (char= c #\-)
      (char= c #\.)
      (char<= #\0 c #\9)
      (is-name-start-char c)
      (= cc #xB7)
      (<= #x0300 cc #x036F)
      (<= #x203F cc #x2040)
      ))

(defun name-char (xml-stream &optional (b (new-buffer)))
  (or (name-start-char xml-stream b)
      (let ((nc (get-char xml-stream nil nil)))
        (when nc
          (if (is-name-char nc)
              (collect-char nc b)
              (unget-char nc xml-stream))))))


;;; Rule [5]: Name

(defun name (xml-stream &optional (b (new-buffer)))
  (let ((nsc (name-start-char xml-stream b)))
    (if nsc
        (labels ((name-char* ()
                   (let ((nc (name-char xml-stream b)))
                     (if nc
                         (name-char*)
                         (copy-buffer b))))
                 )
          (name-char*)))))


;;; Rule [7]: Nmtoken

(defun nmtoken (xml-stream &optional (b (new-buffer)))
  (let ((nsc (name-char xml-stream b)))
    (if nsc
        (labels ((name-char* ()
                   (let ((nc (name-char xml-stream b)))
                     (if nc
                         (name-char*)
                         (copy-buffer b))))
                 )
          (name-char*)))))


;;;; Literals

;;; Rule [9]: EntityValue

(defun entity-value (xml-stream &optional (b)) ; Unoptimized.
  (declare (ignore b))
  (let ((value ()))
    (labels ((double-quoted-rule ()
               (let ((q (get-char xml-stream)))
                 (if (char= q #\")
                     (quoted-rule #\")
                     (unget-char q xml-stream)))
               )

             (single-quoted-rule ()
               (let ((q (get-char xml-stream)))
                 (if (char= q #\')
                     (quoted-rule #\')
                     (unget-char q xml-stream)))
               )

             (quoted-rule (delimiter &optional (b (new-buffer)))
               (let ((c (get-char xml-stream)))
                 (cond ((char= c delimiter) ; End (FIRST FOLLOW)
                        (when (plusp (length b)) (push (copy-buffer b) value))
                        `(entity-value ,@(nreverse value)))

                       ((char= #\& c) ; FIRST Reference
                        (when (plusp (length b)) (push (copy-buffer b) value))
                        (unget-char #\& xml-stream)
                        (let ((reference (reference xml-stream)))
                          (when reference
                            (push reference value)
                            (quoted-rule delimiter)))
                        )

                       ((char= #\% c)
                        (when (plusp (length b)) (push (copy-buffer b) value))
                        (unget-char #\% xml-stream)
                        (let ((pereference (pereference xml-stream)))
                          (when pereference
                            (push pereference value)
                            (quoted-rule delimiter)))
                        )

                       (t ; Other characters...
                        (collect-char c b)
                        (quoted-rule delimiter b))
                       )))
             )
      (or (double-quoted-rule) (single-quoted-rule)))))


;;; Rule [10]: AttValue

(defun att-value (xml-stream &optional (b)) ; Unoptimized.
  (declare (ignore b))
  (let ((value ()))
    (labels ((double-quoted-rule ()
               (let ((q (get-char xml-stream)))
                 (if (char= q #\")
                     (quoted-rule #\")
                     (unget-char q xml-stream)))
               )

             (single-quoted-rule ()
               (let ((q (get-char xml-stream)))
                 (if (char= q #\')
                     (quoted-rule #\')
                     (unget-char q xml-stream)))
               )

             (quoted-rule (delimiter &optional (b (new-buffer)))
               (let ((c (get-char xml-stream)))
                 (cond ((char= c delimiter) ; End (FIRST FOLLOW)
                        (when (plusp (length b)) (push (copy-buffer b) value))
                        `(att-value ,@(nreverse value)))

                       ((char= #\& c) ; FIRST Reference
                        (when (plusp (length b)) (push (copy-buffer b) value))
                        (unget-char #\& xml-stream)
                        (let ((reference (reference xml-stream)))
                          (when reference
                            (push reference value)
                            (quoted-rule delimiter)))
                        )

                       ((char= #\< c)
                        (error "Found character #\< in ATT-VALUE (Rule [10])."))

                       (t ; Other characters...
                        (collect-char c b)
                        (quoted-rule delimiter b))
                       )))
             )
      (or (double-quoted-rule) (single-quoted-rule)))))


;;; Rule [11]: SystemLiteral

(defun system-literal (xml-stream &optional (b)) ; Unoptimized.
  (declare (ignore b))
  (let ((value ()))
    (labels ((double-quoted-rule ()
               (let ((q (get-char xml-stream)))
                 (if (char= q #\")
                     (quoted-rule #\")
                     (unget-char q xml-stream)))
               )

             (single-quoted-rule ()
               (let ((q (get-char xml-stream)))
                 (if (char= q #\')
                     (quoted-rule #\')
                     (unget-char q xml-stream)))
               )

             (quoted-rule (delimiter &optional (b (new-buffer)))
               (let ((c (get-char xml-stream)))
                 (cond ((char= c delimiter) ; End (FIRST FOLLOW)
                        (push (copy-buffer b) value)
                        `(system-literal ,value))

                       (t ; Other characters...
                        (collect-char c b)
                        (quoted-rule delimiter b))
                       )))
             )
      (or (double-quoted-rule) (single-quoted-rule)))))


;;; Rule [12]: PubidLiteral

(defun pubid-literal (xml-stream &optional (b)) ; Unoptimized.
  (declare (ignore b))
  (let ((value ()))
    (labels ((double-quoted-rule ()
               (let ((q (get-char xml-stream)))
                 (if (char= q #\")
                     (quoted-rule #\")
                     (unget-char q xml-stream)))
               )

             (single-quoted-rule ()
               (let ((q (get-char xml-stream)))
                 (if (char= q #\')
                     (quoted-rule #\')
                     (unget-char q xml-stream)))
               )

             (quoted-rule (delimiter &optional (b (new-buffer)))
               (let ((c (get-char xml-stream)))
                 (cond ((char= c delimiter) ; End (FIRST FOLLOW)
                        (push (copy-buffer b) value)
                        `(pubid-literal ,@value))

                       (t
                        ;; Other characters...
                        (unget-char c xml-stream)
                        (let ((pc (pubid-char xml-stream)))
                          (cond (pc
                                 (collect-char pc b)
                                 (quoted-rule delimiter b))
                                (t
                                 (error "Expected a PubidChar, got a ~A (Rule [12])."
                                        pc))))
                        ))))
             )
      (or (double-quoted-rule) (single-quoted-rule)))))


;;; Rule [13]: PubidChar

(defun is-pubid-char (c &aux (cc (char-code c)))
  (or (= cc #x20)
      (= cc #xD)
      (= cc #xA)
      (char<= #\a c #\z)
      (char<= #\A c #\Z)
      (char<= #\0 c #\9)
      (find c "-'()+,./:=?;!*#@$_%" :test #'char=)
      ))

(defun pubid-char (xml-stream &optional (b (new-buffer)))
  (let ((pc (get-char xml-stream)))
    (if (is-pubid-char pc)
        (collect-char pc b)
        (unget-char pc xml-stream))))
        

;;;; 2.4 Character Data and Markup

;;; Rule [14]: CharData

(defun chardata (xml-stream &optional (b (new-buffer)))
  (let ((nc (get-char xml-stream nil *eof*)))
    (case nc
      (#\]
       (let ((nnc (get-char xml-stream nil *eof*)))
         (if (char= nnc #\])
             (let ((nnnc (get-char xml-stream nil *eof*)))
               (if (char= nnnc #\>)
                   (error "Found ']]>' in CharData (Rule [15]).")
                   (progn
                     (unget-char nnnc xml-stream)
                     (collect-string "]]" b)
                     (chardata xml-stream b))))
             (progn
               (collect-char nc b)
               (collect-char nnc b)
               (chardata xml-stream b)))))

      ((#\< #\&) (unget-char nc xml-stream) b)

      (t (if (is-eof nc)
             b
             (progn
               (collect-char nc b)
               (chardata xml-stream b)))))
    ))


;;;; 2.5 Comments

;;; Rule [15]: Comment

(defun comment-rule (xml-stream &optional (comment (new-buffer)))
  ;; Very hairy function...  careful with the state of the backlogged stream.
  (labels ((start ()
             (when (read-chars= "<!--" xml-stream)
               (collect-string "<!--" comment)
               (collect-comment-chars)))
           
           (collect-comment-chars ()
             (let ((c (get-char xml-stream)))
               (if (char= c #\-)
                   (cond ((read-chars= "->" xml-stream)
                          (collect-string "-->" comment)
                          (return-from collect-comment-chars
                            `(comment ,(copy-buffer comment))))
                         ((read-char= #\- xml-stream nil)
                          (get-char xml-stream) ; Consume the second
                                                ; character read in
                                                ; the previous clause.
                          (error "String '--' found in comment."))
                         (t (collect-char c comment)
                            (collect-char (get-char xml-stream) comment)))
                   (collect-char c comment)))
             (collect-comment-chars))
           )
    (start)))


;;;; 2.6 Processing Instructions

;;; Rule [16]: PI
;;;
;;; Need to check that <?xml ...?> is ruled out.

(defun pi-rule (xml-stream &optional (pi-content (new-buffer)))
  (let ((pi-target ""))
    (labels ((start ()
               (when (read-chars= "<?" xml-stream t)
                 (setf pi-target (name xml-stream))
                 (pi-rest)))

             (pi-rest ()
               (let ((ws (white-space xml-stream)))
                 (if ws
                     (pi-content)
                     (error "Expected a white space, got ~A (Rule [16])."
                            ws))))

             (pi-content ()
               (let ((c (get-char xml-stream)))
                 (cond ((char= c #\?)
                        (let ((nc (get-char xml-stream)))
                          (cond ((char= nc #\>)
                                 `(pi ,pi-target ,(copy-buffer pi-content)))
                                (t
                                 (collect-char c pi-content)
                                 (pi-content)))))
                       (t
                        (collect-char c pi-content)
                        (pi-content))
                     )))
             )
      (start))))


;;;; 2.7 CDATA Sections

;;; Rule [18]: CDSect

(defun cdsect (xml-stream &optional (b))
  (declare (ignore b))
  (when (cdstart xml-stream)
    (let ((cdata-chars (cdata xml-stream)))
      (when cdata-chars
        (let ((end (cdend xml-stream)))
          (when end
            `(cdsect ,cdata-chars)))))))


;;; Rule [19]: CDStart

(defun cdstart (xml-stream &optional (b))
  (declare (ignore b))
  (read-chars= "<![CDATA[" xml-stream))


;;; Rule [20]: CData

(defun cdata (xml-stream &optional (b (new-buffer)))
  (labels ((cdata-chars ()
             (cond ((read-chars= "]]>" xml-stream)
                    (unget-chars "]]>" xml-stream)
                    `(cdata ,(copy-buffer b)))
                   (t
                    (collect-char (get-char xml-stream) b)
                    (cdata-chars))))
           )
    (cdata-chars)))


;;; Rule [21]: CDEnd

(defun cdend (xml-stream &optional (b))
  (declare (ignore b))
  (read-chars= "]]>" xml-stream))


;;;; 2.8 Prolog and Document Type Declaration

;;; Prolog

;;; Rule [22]: prolog

(defun prolog (xml-stream &optional (b (new-buffer)))
  (declare (ignore b))
  (let ((xml-pi nil)
        (misc* nil)
        (doctypedecl nil)
        )
    (s xml-stream)
    (setf xml-pi (xmldecl xml-stream))
    (setf misc* (misc* xml-stream))
    (setf doctypedecl (doctypedecl xml-stream))
    `(prolog ,xml-pi ,misc* ,doctypedecl ,(misc* xml-stream))
    ))


;;; Rule [23]: XMLDecl

(defun xmldecl (xml-stream &optional (b))
  (declare (ignore b))
  (let ((version-info nil)
        (encoding-decl nil)
        (sd-decl nil)
        )
    (when (read-chars= "<?xml" xml-stream)
      (when (setf version-info (versioninfo xml-stream))
        (setf encoding-decl (encodingdecl xml-stream))
        (setf sd-decl (sddecl xml-stream))
        (s xml-stream)
        (when (read-chars= "?>" xml-stream)
          `(xmldecl ,version-info
                    ,encoding-decl
                    ,sd-decl))))))


;;; Rule [24]: Versioninfo

(defun versioninfo (xml-stream &optional (b (new-buffer)))
  (labels ((version-num-q (delim)
             (when (read-chars= "1." xml-stream)
               (collect-string "1." b)
               (sub-version-q+ delim)))
           
           (sub-version-q+ (delim)
             (let ((d (get-char xml-stream)))
               (when (digit-char-p d 10)
                 (collect-char d b)
                 (sub-version-q* delim))))

           (sub-version-q* (delim)
             (let ((d (get-char xml-stream)))
               (cond ((digit-char-p d 10)
                      (collect-char d b)
                      (sub-version-q* delim))
                     ((char= d delim)
                      (copy-buffer b))
                     (t nil))))
           )
             
    (when (s xml-stream)
      (when (read-chars= "version" xml-stream)
        (when (eq-rule xml-stream)
          (let ((q (get-char xml-stream)))
            (cond ((or (char= #\' q) (char= #\" q))
                   `(versioninfo ,(version-num-q q)))
                  (t (unget-char q xml-stream)))))))))


;;; Rule [25]: Eq

(defun eq-rule (xml-stream &optional (b))
  (declare (ignore b))
  (s xml-stream)
  (when (read-char= #\= xml-stream)
    (s xml-stream)
    (list 'eq)))


;;; Rule [26]: VersionNum
;;; Inlined in Rule [24]

;;; Rule [27]: Misc*

(defun misc (xml-stream &optional (b))
  (declare (ignore b))
  (or (comment-rule xml-stream)
      (pi-rule xml-stream)
      (s xml-stream)))


(defun misc* (xml-stream &optional (b (new-buffer)))
  (declare (ignore b))
  ;; Misc* is FOLLOWed by a space, the eof, a "<!DOCTYPE" or a regular element.
  ;; I.e., not a comment or a PI.
  (let ((misc ()))
    (labels ((start ()
               (more-misc*))

             (finish-misc ()
               `(misc ,@(nreverse misc)))

             (more-misc* ()
               (let ((nc (get-char xml-stream nil *eof*)))
                 (cond ((eq nc *eof*)
                        `(misc ,@(nreverse misc)))

                       ((is-white-space nc)
                        (unget-char nc xml-stream)
                        (push (misc xml-stream) misc)
                        (more-misc*))

                       ((char= nc #\<)
                        (unget-char #\< xml-stream)
                        (let ((m (misc xml-stream)))
                          (cond (m (push m misc)
                                   (more-misc*))
                                (t (finish-misc)))))

                       (t (unget-char nc xml-stream))
                       )))
             )
      (start))))


;;; Document Type Definition

;;; Rule [28]: doctypedecl

(defun doctypedecl (xml-stream &optional (b (new-buffer)))
  (declare (ignore b))
  (read-chars= "<!DOCTYPE" xml-stream)
  (s xml-stream)
  (let ((name nil)
        (external-id nil)
        (int-subset nil)
        )
    (labels ((start ()
               (when (setf name (name xml-stream))
                 (rest-doctypedecl)
                 (finish)))

             (finish ()
               (list 'doctype name external-id int-subset))
             
             (rest-doctypedecl ()
               (s xml-stream)
               (cond ((read-char= #\[ xml-stream nil)
                      (when (setf int-subset (intsubset xml-stream))
                        (read-char= #\] xml-stream)
                        (s xml-stream)
                        (read-char= #\> xml-stream)))

                     ((read-char= #\> xml-stream nil))

                     (t
                      (when (setf external-id (external-id xml-stream))
                        (rest-doctypedecl)))
                     ))
             )
      (start))))


;;; Rule [28a]: DeclSep

(defun declsep (xml-stream &optional (b))
  (declare (ignore b))
  (let ((ds (or (pereference xml-stream) (s xml-stream))))
    (when ds
      `(declsep ,ds))))


;;; Rule [28b]: intSubset

(defun intsubset (xml-stream &optional (b))
  (declare (ignore b))
  (let ((sss ()))
    (labels ((start ()
               (let ((ss (or (markupdecl xml-stream)
                             (declsep xml-stream))))
                 (cond (ss
                        (push ss sss)
                        (start))
                       (t
                        (and sss `(intsubset ,@(nreverse sss)))))))
             )
      (start))))

#+old-version
(defun intsubset (xml-stream &optional (b))
  (declare (ignore b))
  (let ((ss ()))
    (labels ((start ()
               (s))

             (s ()
               (let ((s (or (markupdecl xml-stream)
                            (declsep xml-stream))))
                 (cond (s
                        (push s ss)
                        (s))
                       (t
                        (and ss `(intsubset ,@ss))))))
             )
      (start))))


;;; Rule [29]: markupDecl

(defun markupdecl (xml-stream &optional (b (new-buffer)))
  (declare (ignore b))
  (let ((m (or (elementdecl xml-stream)
               (attlistdecl xml-stream)
               (entitydecl xml-stream)
               (notationdecl xml-stream)
               (pi-rule xml-stream)
               (comment-rule xml-stream)))
        )
    (when m
      (list 'markupdecl m))
    ))


;;;; 2.9 Standalone Document Declaration

;;; Standalone Documents

(defun sddecl (xml-stream &optional (b))
  (declare (ignore b))
  (when (s xml-stream)
    (when (read-chars= "standalone" xml-stream)
      (when (eq-rule xml-stream)
        (let ((q (get-char xml-stream)))
          (case q
            ((#\' #\")
             (let ((y-or-n (or (read-chars= "yes" xml-stream)
                               (read-chars= "no" xml-stream))))
               (when y-or-n
                 (when (read-char= q xml-stream)
                   `(standalone ,y-or-n)))))
            (t nil))))))
  )


;;;; 3 Logical Structures

;;; Rule [39]: element
;;; The rule would require too much backtrack in order to deal with
;;; "empty" tags, so it is massaged appropriatedly.

(defun element-rule (xml-stream &optional (b))
  (declare (ignore b))
  (let ((et (element-tag-rule xml-stream)))
    (when et
      (if (is-empty-tag et)
          et
          (let ((content (content xml-stream)))
            (when content
              (let ((end-tag (etag xml-stream)))
                (when (and end-tag
                           (equal (end-tag-name end-tag)
                                  (start-tag-name et)))
                  `(element ,et ,content)))))))
    ))


;;; Start-Tags, End-Tags, and Empty-Element Tags

;;; Rule [40] STag
;;; Rule [44] EmptyElementTag

(defun element-tag-rule (xml-stream &optional (b))
  (declare (ignore b))
  (when (read-char= #\< xml-stream)
    (let ((n (name xml-stream)))
      (when n
        (let ((attributes (attribute* xml-stream)))
          (when attributes
            (s xml-stream)
            (let ((empty-tag-p (read-chars= "/>" xml-stream)))
              (if empty-tag-p
                  `(element t ,n ,attributes)
                  (when (read-char= #\> xml-stream)
                    `(tag nil ,n ,attributes)))
              )))))))


(defun is-empty-tag (element-list-rep)
  (second element-list-rep))

(defun start-tag-name (element-list-rep)
  (third element-list-rep))


;;; Rule [41]: Attribute

(defun attribute* (xml-stream &optional (b))
  (declare (ignore b))
  (labels ((attribute ()
             (let ((n (name xml-stream)))
               (when n
                 (when (eq-rule xml-stream)
                   (let ((att-value (att-value xml-stream)))
                     (when att-value
                       `(attribute ,n ,att-value))))))
             )
           
           (attributes ()
             (let* ((s (s xml-stream))
                    (nc (look-at-char nil xml-stream))
                    )
               (if s
                   (if (or (char= #\> nc) (char= #\/ nc))
                       t
                       (let ((attval (attribute))
                             (rest-attributes (attributes))
                             )
                         (if (eq rest-attributes t)
                             (list attval)
                             (cons attval rest-attributes))))
                   (or (char= #\> nc) (char= #\/ nc)))
               ))
           (start ()
             (attributes))
           )
    (start)
    ))
                  


;;; End-Tag

;;; Rule [42]: ETag

(defun etag (xml-stream &optional (b))
  (declare (ignore b))
  (when (read-chars= "</" xml-stream)
    (let ((n (name xml-stream)))
      (when n
        (s xml-stream)
        (when (read-char= #\> xml-stream)
          `(end-tag ,n))))))


(defun end-tag-name (element-list-rep)
  (second element-list-rep))


;;; Content of Elements

;;; Rule [43] content

(defun content (xml-stream &optional (b))
  (declare (ignore b))
  (labels ((collect-content (content-list)
             ;; Let's dispatch on FIRST.
             (let ((nc (look-at-char nil xml-stream nil *eof*)))
               (case nc
                 (#\& 
                  (let* ((r (reference xml-stream))
                         (cd (chardata xml-stream))
                         )
                    (cond ((and (null r) (null cd)) ; Should never happen!
                           (error "Ampersand in element content does not start an entity."))
                          ((null cd)
                           (collect-content (cons cd content-list)))
                          ((null r)
                           (error "Ampersand in element content does not start an entity."))
                          (t
                           (collect-content (list* cd r content-list)))
                          ))
                  )
                               
                 (#\< ; Must check whether we have an end tag
                  (if (read-chars= "</" xml-stream)
                      (progn
                        (unget-chars "</" xml-stream)
                        content-list)
                      (let* ((ne (or (cdsect xml-stream)
                                     (pi-rule xml-stream)
                                     (comment-rule xml-stream)
                                     (element-rule xml-stream)))
                             (cd (chardata xml-stream))
                             )
                        (cond ((and (null ne) (null cd)) ; Should never happen!
                               (warn "Less-than sign found in content; trying to skip ahead.")
                               (get-char xml-stream)
                               (collect-content content-list))
                              ((null ne)
                               (warn "Less-than sign found in content; trying to skip ahead.")
                               (collect-content (cons cd content-list)))
                              ((null cd)
                               (collect-content (cons ne content-list)))
                              (t
                               (collect-content (list* cd ne content-list))))))
                  )
                 (t
                  (if (is-eof nc)
                      (error "End of file while reading element content.")
                      (progn
                        (warn "Strange character ib element content: ~S; trying to skip ahead" nc)
                        (get-char xml-stream)
                        (collect-content content-list)))
                  )
                 )))
           )
  (let ((chardata (chardata xml-stream)))
    (nreverse (collect-content (and chardata (list chardata)))))
  ))


;;; Rule [45]: elementDecl

(defun elementdecl (xml-stream &optional (b))
  (declare (ignore b))
  (let ((n nil)
        (content-spec nil)
        )
    (when (read-chars= "<!ELEMENT" xml-stream)
      (when (s xml-stream)
        (when (setf n (name xml-stream))
          (when (s xml-stream)
            (when (setf content-spec (contentspec xml-stream))
              (s xml-stream)
              (when (read-char= #\> xml-stream)
                `(elementdecl ,n ,content-spec)))))))
    ))


;;; Rule [46]: contentspec

(defun contentspec (xml-stream &optional (b))
  (declare (ignore b))
  (or (read-chars= "EMPTY" xml-stream)
      (read-chars= "ANY" xml-stream)
      (mixed xml-stream)
      (children xml-stream)))


;;; 3.2.1 Element content
;;;
;;; Rules 47 to 50 describe a simple RE-like language.  Alas, the
;;; rules are written in a non recursive-descent friendly fashion.
;;; Therefore I rewrite them in order to make them recursive-descent
;;; friendly. (You can figure out whether the result is LL(K) or
;;; whatnot).
;;;
;;; Notes:
;;; XML 1.0 and 1.1 bottom out these rules (and rules 47 to 50) with
;;; the non-teminal 'Name' and then give an example of an ELEMENT
;;; declaration which fails the rule.  The offending example is
;;; hereafter, where the 'Name's start with a '%'. I don't know what to leave in.
;;;
;;; <!ELEMENT dictionary-body (%div.mix; | %dict.mix;)*>
;;;

;;; New Rules
#|

children ::= ncs psf

psf      ::= ('?' | '+' | '*')?

ncs      ::= '(' S? (ncs psf | Name psf) S? rest-ncs

rest-ncs ::= ',' S? seqs ')'
         |   '|' S? alts ')'
         |   ')'

seqs     ::=  (ncs psf | Name psf) S? ',' S? seqs
         |    (ncs psf | Name psf)

alts     ::=  (ncs psf | Name psf) S? '|' S? alts
         |    (ncs psf | Name psf)

|#

;;; New Rules
;;; 'pfs' is folded in...

(defun children (xml-stream &optional (b))
  (declare (ignore b))
  (let ((ncs (ncs xml-stream)))
    (when ncs
      (let ((nc (get-char xml-stream nil *eof*)))
        (case nc
          (#\? `(? ,ncs))
          (#\+ `(+ ,ncs))
          (#\* `(* ,ncs))
          (t (cond ((is-eof nc) ncs)
                   (t (unget-char nc xml-stream)
                      ncs)
                   )))
        ))))


(defun ncs (xml-stream &optional (b)) ; ncs = Name Choice Sequence
  (declare (ignore b))
  (let ((ncs nil)
        (rest-ncs nil)
        )
    (when (read-char= #\( xml-stream nil)
      (s xml-stream)
      (let ((nc (look-at-char nil xml-stream)))
        ;; The code in the two COND branches is actually the same.  I
        ;; leave it like this just to emphasize the simmetry.  It
        ;; could be factored into a local function.
        (cond ((char= nc #\()
               (when (setf ncs (ncs xml-stream))
                 (s xml-stream)
                 (let ((nc (get-char xml-stream)))
                   (case nc
                     (#\? (setf ncs `(? ,ncs)))
                     (#\+ (setf ncs `(+ ,ncs)))
                     (#\* (setf ncs `(* ,ncs)))
                     (t (cond ((is-eof nc) (error "End of file"))
                              (t (unget-char nc xml-stream)
                                 (setf ncs ncs) ; redundant, but symmetric
                                 ))
                        ))
                   )
                 (s xml-stream)
                 (let ((nc (look-at-char nil xml-stream)))
                   (case nc
                     ((#\, #\| #\) )
                      (when (setf rest-ncs
                                  (rest-ncs xml-stream))
                        (if (eq t rest-ncs)
                            ncs
                            (list* (if (char= nc #\|) 'alt 'seq)
                                   ncs
                                   rest-ncs))))
                     (t (unget-char nc xml-stream))))))

              ((is-name-char nc)
               (when (setf ncs (name xml-stream))
                 (s xml-stream)
                 (let ((nc (get-char xml-stream)))
                   (case nc
                     (#\? (setf ncs `(? ,ncs)))
                     (#\+ (setf ncs `(+ ,ncs)))
                     (#\* (setf ncs `(* ,ncs)))
                     (t (cond ((is-eof nc) (error "End of file"))
                              (t (unget-char nc xml-stream)
                                 (setf ncs ncs) ; redundant, but symmetric
                                 ))
                        ))
                   )
                 (s xml-stream)
                 (let ((nc (look-at-char nil xml-stream)))
                   (case nc
                     ((#\, #\| #\) )
                      (when (setf rest-ncs
                                  (rest-ncs xml-stream))
                        (if (eq t rest-ncs)
                            ncs
                            (list* (if (char= nc #\|) 'alt 'seq)
                                   ncs
                                   rest-ncs))))
                     (t (unget-char nc xml-stream))))))
              ))
      )))


(defun rest-ncs (xml-stream &optional (b))
  (declare (ignore b))
  (s xml-stream)
  (let ((nc (get-char xml-stream)))
    (case nc
      (#\, (s xml-stream) (seqs xml-stream))
      (#\| (s xml-stream) (alts xml-stream))
      (#\) t)
      (t (unget-char nc xml-stream)))))


(defun seqs (xml-stream &optional (b))
  (declare (ignore b))
  (let ((e (or (ncs xml-stream)
               (name xml-stream)))
        ;; The OR works because NCS and NAME backtrack on the first
        ;; character.
        (seqs ())
        )
    (s xml-stream)
    (let ((nc (get-char xml-stream)))
      (case nc
        (#\? (setf e `(? ,e)))
        (#\+ (setf e `(+ ,e)))
        (#\* (setf e `(* ,e)))
        (t (cond ((is-eof nc) (error "End of file"))
                 (t (unget-char nc xml-stream)
                    (setf e e) ; redundant, but symmetric
                    ))
           ))
      )
    (s xml-stream)
    (let ((nc (get-char xml-stream)))
      (cond ((char= #\, nc)
             (s xml-stream)
             (when (setf seqs (seqs xml-stream))
               (cons e seqs)))
            ((char= #\) nc)
             (list e))
            (t (unget-char nc xml-stream))))
    ))


(defun alts (xml-stream &optional (b))
  (declare (ignore b))
  (let ((e (or (ncs xml-stream)
               (name xml-stream)))
        ;; The OR works because NCS and NAME backtrack on the first
        ;; character.
        (alts ())
        )
    (s xml-stream)
    (let ((nc (get-char xml-stream)))
      (case nc
        (#\? (setf e `(? ,e)))
        (#\+ (setf e `(+ ,e)))
        (#\* (setf e `(* ,e)))
        (t (cond ((is-eof nc) (error "End of file"))
                 (t (unget-char nc xml-stream)
                    (setf e e) ; redundant, but symmetric
                    ))
           ))
      )
    (s xml-stream)
    (let ((nc (get-char xml-stream)))
      (cond ((char= #\| nc)
             (s xml-stream)
             (when (setf alts (alts xml-stream))
               (cons e alts)))
            ((char= #\) nc)
             (list e))
            (t (unget-char nc xml-stream))))
    ))


;;; Rule [51]: mixed

(defun mixed (xml-stream &optional (b (new-buffer)))
  (declare (ignore b))
  (let ((names ()))
    (labels ((start ()
               (when (read-char= #\( xml-stream)
                 (s xml-stream)
                 (cond ((read-chars= "#PCDATA" xml-stream)
                        (s xml-stream)
                        (let ((nc (get-char xml-stream)))
                          (cond ((char= nc #\|)
                                 (unget-char nc xml-stream)
                                 (pcdata-names)
                                 (s xml-stream) ; Redundant.
                                 (when (read-chars= ")*" xml-stream)
                                   `(mixed (or "#PCDATA" ,@(nreverse names)))))
                                ((string= (string nc) ")" ) ; Kludge for indent.
                                 (if (char= #\* (get-char xml-stream nil #\Null))
                                     (unget-chars ")*" xml-stream)
                                     '(mixed "#PCDATA"))))))
                       (t (unget-char #\( xml-stream))))
               )
             (pcdata-names ()
               (s xml-stream)
               (let ((nc (get-char xml-stream)))
                 (cond ((char= nc #\|)
                        (s xml-stream)
                        (let ((n (name xml-stream)))
                          (when n
                            (push n names)
                            (pcdata-names))))
                       ((is-white-space nc)
                        (pcdata-names))
                       ((string= (string nc) ")" ) ; Kludge for indent.
                        (unget-char nc xml-stream)
                        t)
                       (t (unget-char nc xml-stream)))))
             )
      (start))))

;;; Attribute-list Declaration

;;; Rule [52]: attlistdecl

(defun attlistdecl (xml-stream &optional (b))
  (declare (ignore b))
  (labels ((start ()
             (let ((n nil)
                   (attdefs nil)
                   )
               (when (read-chars= "<!ATTLIST" xml-stream)
                 (when (s xml-stream)
                   (when (setf n (name xml-stream))
                     (when (setf attdefs (attdefs))
                       (s xml-stream)
                       (when (read-char= #\> xml-stream)
                         `(attlist ,n
                                  ,@(unless (eq attdefs t)
                                      attdefs))))))))
               )
           (attdefs ()
             (s xml-stream)
             (let ((nc (look-at-char nil xml-stream)))
               (cond ((char= #\> nc)
                      t)
                     (t
                      (unget-char #\Space xml-stream) ; Hairy.
                      (let ((attdef (attdef xml-stream)))
                        (when attdef
                          (let ((more-attdefs (attdefs)))
                            (when more-attdefs
                              (cons attdef
                                    (unless (eq t more-attdefs)
                                      more-attdefs)))
                            ))))
                     )))
           )
    (start)
    ))


;;; Rule [53]: attdef

(defun attdef (xml-stream &optional (b))
  (declare (ignore b))
  (let ((n nil)
        (att-type nil)
        (default-decl nil)
        )
    (when (s xml-stream) ; at least one.
      (when (setf n (name xml-stream))
        (when (s xml-stream)
          (when (setf att-type (atttype xml-stream))
            (when (s xml-stream)
              (when (setf default-decl (defaultdecl xml-stream))
                `(attdef ,n ,att-type ,default-decl))
              )))))
    ))


;;; Rule [54]: AttType

(defun atttype (xml-stream &optional (b))
  (declare (ignore b))
  (or (stringtype xml-stream)
      (tokenizedtype xml-stream)
      (enumeratedtype xml-stream)))


;;; Rule [55]: StringType

(defun stringtype (xml-stream &optional (b))
  (declare (ignore b))
  (read-chars= "CDATA" xml-stream))


;;; Rule [56]: TokenizedType

(defun tokenizedtype (xml-stream &optional (b))
  (declare (ignore b))
  ;; Re ordered in order to make sure that longest match comes first.
  (or (read-chars= "IDREFS" xml-stream)
      (read-chars= "IDREF" xml-stream)
      (read-chars= "ID" xml-stream)
      (read-chars= "ENTITY" xml-stream)
      (read-chars= "ENTITIES" xml-stream)
      (read-chars= "NMTOKENS" xml-stream)
      (read-chars= "NMTOKEN" xml-stream)))


;;; Enumerated Attribute Types

;;; Rule [57]: EnumeratedType

(defun enumeratedtype (xml-stream &optional (b))
  (declare (ignore b))
  (or (notationtype xml-stream)
      (enumeration xml-stream)))


;;; Rule [58]: NotationType

(defun notationtype (xml-stream &optional (b))
  (declare (ignore b))
  (let ((name nil)
        (more-names nil)
        )
    (labels ((start ()
               (when (read-chars= "NOTATION" xml-stream)
                 (when (s xml-stream)
                   (when (read-char= #\( xml-stream)
                     (s xml-stream)
                     (when (setf name (name xml-stream))
                       (s xml-stream)
                       (when (setf more-names (more-names))
                         (when (read-char= #\) xml-stream)
                           `(notationtype ,name
                                          ,@(if (eq more-names t)
                                                ()
                                                more-names)))))
                     )))
               )
             (more-names ()
               (let ((nc (get-char xml-stream)))
                 (cond ((char= #\| nc)
                        (s xml-stream)
                        (let ((name (name xml-stream)))
                          (when name
                            (s xml-stream)
                            (let ((more-names (more-names)))
                              (cond ((eq more-names t)
                                     (list name))
                                    (more-names
                                     (cons name more-names))
                                    (t nil))))))
                       ((char= #\) nc)
                        (unget-char nc xml-stream)
                        t))))
             )
      (start))))


;;; Rule [59]: Enumeration

(defun enumeration (xml-stream &optional (b))
  (declare (ignore b))
  (labels ((start ()
             (read-char= #\( xml-stream nil)
             (s xml-stream)
             (let ((n (nmtoken xml-stream)))
               (when n
                 (s xml-stream)
                 (let ((more-nmtokens (more-nmtokens)))
                   (when (read-char= #\) xml-stream)
                     `(enumeration ,n ,@(if (eq more-nmtokens t)
                                            ()
                                            more-nmtokens))))))
             )
           (more-nmtokens ()
             (let ((nc (get-char xml-stream)))
               (cond ((char= #\| nc)
                      (s xml-stream)
                      (let ((nt (nmtoken xml-stream)))
                        (when nt
                          (s xml-stream)
                          (let ((more-tokens (more-nmtokens)))
                            (cond ((eq more-tokens t)
                                   (list nt))
                                  (more-tokens
                                   (cons nt more-tokens))
                                  (t nil))))))
                     ((char= #\) nc)
                      (unget-char nc xml-stream)
                      t))))
           )
    (start)))


;;; 3.3.2 Attribute Defaults

;;; Rule [60]: DefaultDecl

(defun defaultdecl (xml-stream &optional (b))
  (declare (ignore b))
  (labels ((attval ()
             (let ((fixed (read-chars= "#FIXED" xml-stream nil)))
               (s xml-stream)
               (let ((attval (att-value xml-stream)))
                 (when attval
                   (if fixed
                       `("#FIXED" ,attval)
                       attval)))))
           )
    (or (and (read-chars= "#REQUIRED" xml-stream nil) "#REQUIRED")
        (and (read-chars= "#IMPLIED" xml-stream nil) "#IMPLIED")
        (attval))))


;;;; 4.1 Characters and Entity References

;;; Rule [66]: CharRef

(defun char-ref (xml-stream &optional (b (new-buffer)))
  (labels ((hexadecimal-rep ()
             (let ((d (get-char xml-stream)))
               (cond ((digit-char-p d 16) ; RADIX 16.
                      (collect-char d b)
                      (hexadecimal-rep))
                     ((char= d #\;)
                      ;; (collect-char d b)
                      ;; (copy-buffer b)
                      `(char-ref ,(copy-buffer b) 16))
                     (t
                      (unget-chars b xml-stream))))
             )
           (decimal-rep ()
             (let ((d (get-char xml-stream)))
               (cond ((digit-char-p d 10) ; RADIX 10.
                      (collect-char d b)
                      (decimal-rep))
                     ((char= d #\;)
                      ;; (collect-char d b)
                      ;; (copy-buffer b)
                      `(char-ref ,(copy-buffer b) 10))
                     (t
                      (unget-chars b xml-stream))))
             )
           (start ()
             (let ((c-amp (get-char xml-stream)))
               (cond ((char= c-amp #\&)
                      ; (collect-char c-amp b)
                      (let ((c-hash (get-char xml-stream)))
                        (cond ((char= c-hash #\#)
                               ; (collect-char c-hash b)
                               (let ((nc (get-char xml-stream)))
                                 (cond ((or (char= nc #\x)
                                            (char= nc #\X))
                                        ; (collect-char nc b)
                                        (hexadecimal-rep))
                                       (t
                                        (unget-char nc xml-stream)
                                        (decimal-rep))
                                       )))
                              (t
                               (unget-char c-hash xml-stream)))))
                     (t
                      (unget-char c-amp xml-stream))
                     )))
           )
    (start)))


;;; Rule [67]: Reference

(defun reference (xml-stream &optional b)
  (declare (ignore b))
  (or (entity-ref xml-stream)
      (char-ref xml-stream)))


;;; Rule [68]: EntityRef

(defun entity-ref (xml-stream &optional (b (new-buffer)))
  (let ((c (get-char xml-stream)))
    (if (char= c #\&)
        (let ((n (name xml-stream b)))
          (cond (n
                  (let ((sc (get-char xml-stream)))
                    (cond ((char= sc #\;)
                           #|
                           (collect-char c b)
                           (collect-string n b)
                           (collect-char sc b)
                           (copy-buffer b)
                           |#
                           `(entity-ref ,n))
                          (t
                           (unget-char sc xml-stream)
                           (unget-chars n xml-stream)
                           (unget-char c xml-stream)))))
                 (t
                  (unget-char c xml-stream))
                 ))
        (unget-char c xml-stream))))


;;; Rule [69]: PEReference

(defun pereference (xml-stream &optional (b (new-buffer)))
  (let ((c (get-char xml-stream)))
    (if (char= c #\%)
        (let ((n (name xml-stream b)))
          (cond (n
                 (let ((sc (get-char xml-stream)))
                   (cond ((char= sc #\;)
                          `(pereference ,n))
                         (t
                          (unget-char sc xml-stream)
                          (unget-chars n xml-stream)
                          (unget-char c xml-stream)))))
                (t
                 (unget-char c xml-stream))))
        (unget-char c xml-stream))))


;;; 4.2 Entity Declarations

;;; Entity Declaration

;;; Rule [70]: EntityDecl
;;;
;;; The rule has been changed given the common prefix "<!ENTITY".

(defun entitydecl (xml-stream &optional (b))
  (declare (ignore b))
  (labels ((ge-or-pe ()
             (let ((nc (look-at-char nil xml-stream)))
               (if (char= nc #\%)
                   (pedecl xml-stream)
                   (gedecl xml-stream))))
           )
    (when (read-chars= "<!ENTITY" xml-stream nil)
      (when (s xml-stream)
        (ge-or-pe)))))


;;; Rule [71]: GEDecl
;;; Modificata

(defun gedecl (xml-stream &optional (b))
  (declare (ignore b))
  (let ((n (name xml-stream)))
    (when n
      (when (s xml-stream)
        (let ((ed (entitydef xml-stream)))
          (when ed
            (s xml-stream)
            (when (read-char= #\> xml-stream)
              `(gedecl ,n ,ed))))))
    ))


;;; Rule [72]: PEDecl
;;; Modificata

(defun pedecl (xml-stream &optional (b))
  (declare (ignore b))
  (when (read-char= #\% xml-stream)
    (when (s xml-stream)
      (let ((n (name xml-stream)))
        (when n
          (when (s xml-stream)
            (let ((ped (pedef xml-stream)))
              (when ped
                (s xml-stream)
                (when (read-char= #\> xml-stream)
                  `(pedecl ,n ,ped))))))
        ))))


;;; Rule [73]: EntityDef    

(defun entitydef (xml-stream &optional (b))
  (declare (ignore b))
  (let ((ev (entity-value xml-stream)))
    (if ev
        `(entitydef ,ev)
        (let ((eid (external-id xml-stream)))
          (when eid
            (let ((ndd (ndatadecl xml-stream)))
              (when ndd
                `(entitydef ,eid ,(if (eq t ndd) () ndd))))))
        )))


;;; Rule [74]: PEDef

(defun pedef (xml-stream &optional (b))
  (declare (ignore b))
  (or (entity-value xml-stream)
      (external-id xml-stream)))


;;; Rule [75]: ExternalID

(defun external-id (xml-stream &optional (b (new-buffer)))
  (declare (ignore b))
  (let ((sid nil)
        (pid nil)
        )
    (cond ((read-chars= "SYSTEM" xml-stream)
           (s xml-stream)
           (when (setf sid (system-literal xml-stream))
             `(external-id system ,sid)))

          ((read-chars= "PUBLIC" xml-stream)
           (s xml-stream)
           (when (setf pid (pubid-literal xml-stream))
             (s xml-stream)
             (if (setf sid (system-literal xml-stream))
                 `(external-id public ,pid ,sid)
                 (progn ; Necessary because of the interplay with 'public-id'.
                   (unget-char #\Space xml-stream)
                   (unget-char #\" xml-stream)
                   (unget-chars (second pid) xml-stream)
                   (unget-char #\" xml-stream)
                   (unget-char #\Space xml-stream)
                   (unget-chars "PUBLIC" xml-stream)
                   nil)))
           )
          )))


;;; Rule [76]: NDataDecl

(defun ndatadecl (xml-stream &optional (b))
  (declare (ignore b))
  (when (s xml-stream)
    (let ((nc (look-at-char nil xml-stream)))
      (if (char= nc #\>)
          t
          (when (read-chars= "NDATA" xml-stream)
            (when (s xml-stream)
              (let ((n (name xml-stream)))
                (when n
                  `(ndata ,n))))))
      )))


;;;; 4.3.3 Character Encoding in Entities

;;; Encoding Declaration

;;; Rule [80]: EncodingDecl

(defun encodingdecl (xml-stream &optional (b))
  (declare (ignore b))
  (when (s xml-stream)
    (when (read-chars= "encoding" xml-stream)
      (when (eq-rule xml-stream)
        (let ((en (encname xml-stream)))
          (when en
            `(encoding ,en))))))
  )


;;; Rule [81]: EncName
;;; Non stanrdad coding of this rule.

(defun encname (xml-stream &optional (b (new-buffer)))
  (labels ((double-quoted-rule ()
             (let ((q (get-char xml-stream)))
               (if (char= q #\")
                   (quoted-rule #\")
                   (unget-char q xml-stream)))
             )

           (single-quoted-rule ()
             (let ((q (get-char xml-stream)))
               (if (char= q #\')
                   (quoted-rule #\')
                   (unget-char q xml-stream)))
             )

           (quoted-rule (delimiter)
             (let ((c (get-char xml-stream)))
               (if (char= c delimiter) ; End (FIRST FOLLOW)
                   (copy-buffer b)
                   (progn
                     (collect-char c b)
                     (quoted-rule delimiter))))
             )

           (quoted-name ()
             (or (double-quoted-rule)
                 (single-quoted-rule)))
           )
    (let ((en (quoted-name)))
      (when (and en
                 (every #'is-latin-character en)
                 (is-latin-alphabetic-character (char en 0))
                 )
        en))))


;;;; 4.7 Notation Declarations

;;; Rule [82]: NotationDecl

(defun notationdecl (xml-stream &optional (b))
  (declare (ignore b))
  (when (read-chars= "<!NOTATION" xml-stream)
    (when (s xml-stream)
      (let ((n (name xml-stream)))
        (when n
          (when (s xml-stream)
            ;; The rules for ExternalID and PublicID overlap; they
            ;; should be fixed. Alas, I may backtrack a lot here.
            (let ((eid (external-id xml-stream)))
              (cond (eid
                     (s xml-stream)
                     (when (read-char= #\> xml-stream)
                       `(notation ,n ,eid)))
                    (t (let ((pid (public-id xml-stream)))
                         (when pid
                           (s xml-stream)
                           (when (read-char= #\> xml-stream)
                             `(notation ,n ,pid)))))
                    )))))
      )))

;;; Rule [83]: PublicID

(defun public-id (xml-stream &optional (b))
  (declare (ignore b))
  (let ((pid nil))
    (when (read-chars= "PUBLIC" xml-stream)
      (when (s xml-stream)
        (when (setf pid (pubid-literal xml-stream))
          `(public-id ,pid))))))


;;;;===========================================================================
;;;; Utility functions.

(defun read-char= (=c=
                   &optional
                   (xml-stream *standard-input*)
                   (errorp t)
                   (eof-error-p t)
                   eof
                   recursive)
  (declare (type character =c=)
           (type (or stream (eql t)) xml-stream)
           (type boolean errorp eof-error-p recursive)
           )
  (let ((c (get-char xml-stream eof-error-p eof recursive))) ; Note the GET-CHAR.
    (cond ((and errorp (eq c eof)) ; ... and (NOT EOF-ERROR-P)
           nil)

          ((and errorp (char/= =c= c))
           (error "Parse error: seen ~C, expected ~C." c =c=))

          ((char/= =c= c)
           (unget-char c xml-stream)
           nil)

          (t c))))


(defun read-chars= (=s=
                    &optional
                    (xml-stream *standard-input*)
                    (eof-error-p t)
                    eof
                    recursive)
  (declare (type string =s=)
           (type (or stream (eql t)) xml-stream)
           (type boolean eof-error-p recursive)
           )
  (loop for c of-type character across =s=
        for rc = (read-char= c xml-stream nil eof-error-p eof recursive)
        if rc
        collect rc into rcs
        else
        do (progn (unget-chars rcs xml-stream)
             (return-from read-chars= nil)))
  =s=)


;;;---------------------------------------------------------------------------
;;; Entry points...

(defgeneric parse (source &key &allow-other-keys)

  (:documentation "Parse a XML source and returns a 'document'.

Notes:

This is an experimental parser for XML.")

  (:method ((source string) &key &allow-other-keys)
   (with-input-from-string (s source)
     (parse s)))

  (:method ((source pathname) &key &allow-other-keys)
   (with-open-file (in source :direction :input)
     (parse in)))

  (:method ((source stream) &key &allow-other-keys)
   (document-rule source))
  )

;;;; end of file -- xml-parser.lisp --
