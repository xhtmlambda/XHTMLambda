(X)HTMLambda
============
Marco Antoniotti
See file COPYING for licensing information



DESCRIPTION
-----------

(X)HTMLambda is yet another (X)HTML library which emphasizes
programmability and user-friendliness.  Each (X)HTML element is a
strucuterd object and pretty-printing of (X)HTML trees is well defined
to provide properly indented human-readable output even for complex
recursive arrangements.


INSTALLATION
------------

(X)HTMLambda is available on quicklisp.

Otherwise, installation should be simple if you have ASDF,
or MK-DEFSYSTEM set up.


A NOTE ON FORKING
-----------------

Of course you are free to fork the project subject to the current
licensing scheme.  However, before you do so, I ask you to consider
plain old "cooperation" by asking me to become a developer.
It helps keeping the entropy level at an acceptable level.


Enjoy

